//
//  CountryWiseModel.swift
//
//  Created by Hamza Hasan on 01/05/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class CountryWiseModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lat = "Lat"
    static let country = "Country"
    static let active = "Active"
    static let lon = "Lon"
    static let city = "City"
    static let recovered = "Recovered"
    static let province = "Province"
    static let cityCode = "CityCode"
    static let countryCode = "CountryCode"
    static let date = "Date"
    static let deaths = "Deaths"
    static let confirmed = "Confirmed"
  }

  // MARK: Properties
  @objc dynamic var lat: String? = ""
  @objc dynamic var country: String? = ""
  @objc dynamic var active = 0
  @objc dynamic var lon: String? = ""
  @objc dynamic var city: String? = ""
  @objc dynamic var recovered = 0
  @objc dynamic var province: String? = ""
  @objc dynamic var cityCode: String? = ""
  @objc dynamic var countryCode: String? = ""
  @objc dynamic var date: String? = ""
  @objc dynamic var deaths = 0
  @objc dynamic var confirmed = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "date"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    lat <- map[SerializationKeys.lat]
    country <- map[SerializationKeys.country]
    active <- map[SerializationKeys.active]
    lon <- map[SerializationKeys.lon]
    city <- map[SerializationKeys.city]
    recovered <- map[SerializationKeys.recovered]
    province <- map[SerializationKeys.province]
    cityCode <- map[SerializationKeys.cityCode]
    countryCode <- map[SerializationKeys.countryCode]
    date <- map[SerializationKeys.date]
    deaths <- map[SerializationKeys.deaths]
    confirmed <- map[SerializationKeys.confirmed]
  }


}
