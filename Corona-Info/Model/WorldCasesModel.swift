//
//  WorldCasesModel.swift
//  Corona-Info
//
//  Created by Mac Book on 01/05/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class WorldCasesModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let totalConfirmed = "TotalConfirmed"
    static let totalDeaths = "TotalDeaths"
    static let totalRecovered = "TotalRecovered"
  }

  // MARK: Properties
  @objc dynamic var totalConfirmed = 0
  @objc dynamic var totalDeaths = 0
  @objc dynamic var totalRecovered = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "totalConfirmed"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    totalConfirmed <- map[SerializationKeys.totalConfirmed]
    totalDeaths <- map[SerializationKeys.totalDeaths]
    totalRecovered <- map[SerializationKeys.totalRecovered]
  }
}
