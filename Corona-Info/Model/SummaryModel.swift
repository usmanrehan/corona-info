//
//  SummaryModel.swift
//
//  Created by Hamza Hasan on 01/05/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class SummaryModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let totalConfirmed = "TotalConfirmed"
    static let recentRecovered = "NewRecovered"
    static let totalDeaths = "TotalDeaths"
    static let totalRecovered = "TotalRecovered"
    static let recentDeaths = "NewDeaths"
    static let recentConfirmed = "NewConfirmed"
  }

  // MARK: Properties
  @objc dynamic var totalConfirmed = 0
  @objc dynamic var recentRecovered = 0
  @objc dynamic var totalDeaths = 0
  @objc dynamic var totalRecovered = 0
  @objc dynamic var recentDeaths = 0
  @objc dynamic var recentConfirmed = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "totalConfirmed"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    totalConfirmed <- map[SerializationKeys.totalConfirmed]
    recentRecovered <- map[SerializationKeys.recentRecovered]
    totalDeaths <- map[SerializationKeys.totalDeaths]
    totalRecovered <- map[SerializationKeys.totalRecovered]
    recentDeaths <- map[SerializationKeys.recentDeaths]
    recentConfirmed <- map[SerializationKeys.recentConfirmed]
  }


}
