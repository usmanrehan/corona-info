
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class CountryModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let Country = "Country"
        static let ISO2 = "ISO2"
        static let Slug = "Slug"
    }
    
    // MARK: Properties
    @objc dynamic var Country:String = ""
    @objc dynamic var ISO2:String = ""
    @objc dynamic var Slug:String = ""

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "ISO2"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        Country <- map[SerializationKeys.Country]
        ISO2 <- map[SerializationKeys.ISO2]
        Slug <- map[SerializationKeys.Slug]
    }
}
