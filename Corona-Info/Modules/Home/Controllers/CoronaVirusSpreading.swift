//
//  CoronaVirusSpreading.swift
//  Corona-Info
//
//  Created by Mac Book on 01/05/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper

class CoronaVirusSpreading: BaseController {

    @IBOutlet weak var lblTotalNumberOfDeaths: UILabel!
    @IBOutlet weak var lblTotalNumberOfCures: UILabel!
    @IBOutlet weak var lblSpreadingPridiction: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getWorldCases()
        // Do any additional setup after loading the view.
    }

}
extension CoronaVirusSpreading{
    private func setData(data:WorldCasesModel){
        self.lblTotalNumberOfDeaths.text = "\(data.totalDeaths)"
        self.lblTotalNumberOfCures.text = "\(data.totalRecovered)"
        
        let spreadinPercentage = ((Double(data.totalDeaths)/Double(data.totalConfirmed))*100.0).rounded()
        self.lblSpreadingPridiction.text = "\(spreadinPercentage)%"
    }
}
extension CoronaVirusSpreading{
    private func getWorldCases(){
        APIManager.sharedInstance.usersAPIManager.GetWorldCases(params: [:], success: { (responseObject) in
            print(responseObject)
            guard let worldCases = Mapper<WorldCasesModel>().map(JSON: responseObject) else{return}
            self.setData(data: worldCases)
        }) { (error) in
            print(error)
        }
    }
}
