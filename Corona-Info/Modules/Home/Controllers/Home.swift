//
//  Home.swift
//  Corona-Info
//
//  Created by Mac Book on 01/05/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit

class Home: BaseController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Covid19 Info App"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnOption(_ sender: UIButton) {
        switch sender.tag{
        case 0:
            super.pushToGlobalSummary()
        case 1:
            super.pushToCountries()
        case 2:
            super.pushToCoronaVirusSpreading()
        case 3:
            super.pushToVentilatorStatus()
        default:
            super.pushToVaccines()
        }
    }
}
