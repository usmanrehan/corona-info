//
//  CountryTVC.swift
//  Corona-Info
//
//  Created by Mac Book on 01/05/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit

class CountryTVC: UITableViewCell {

    @IBOutlet weak var lblCountry: UILabel!
    
    func setData(data:CountryModel){
        self.lblCountry.text = data.Country
    }
}
