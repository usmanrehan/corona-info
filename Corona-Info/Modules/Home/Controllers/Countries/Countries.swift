//
//  Countries.swift
//  Corona-Info
//
//  Created by Mac Book on 01/05/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper

class Countries: BaseController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfSearchField: UISearchBar!
    
    var arrCountries = [CountryModel]()
    var arrFilterCountries = [CountryModel]()
    
    var isFilter = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCountries()
        // Do any additional setup after loading the view.
    }

}
extension Countries: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty{
            self.isFilter = false
        }
        else{
            self.isFilter = true
            self.arrFilterCountries = self.arrCountries.filter{($0.Country).localizedCaseInsensitiveContains(searchText)}
        }
        self.tableView.reloadData()
    }
    
}
extension Countries{
    private func getCountries(){
        APIManager.sharedInstance.usersAPIManager.GetCountries(params: [:], success: { (responseObject) in
            print(responseObject)
            guard let countries = responseObject as? [[String:Any]] else {return}
            self.arrCountries = Mapper<CountryModel>().mapArray(JSONArray: countries)
            self.tableView.reloadData()
        }) { (error) in
            print(error)
        }
    }
}
extension Countries: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.isFilter{
        case true:
            return self.arrFilterCountries.count
        case false:
            return self.arrCountries.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.isFilter{
        case true:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CountryTVC", for: indexPath) as! CountryTVC
            let data = self.arrFilterCountries[indexPath.row]
            cell.setData(data:data)
            return cell
        case false:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CountryTVC", for: indexPath) as! CountryTVC
            let data = self.arrCountries[indexPath.row]
            cell.setData(data:data)
            return cell
        }
    }
}
extension Countries: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedCountry = CountryModel()
        switch self.isFilter{
        case true:
            selectedCountry = self.arrFilterCountries[indexPath.row]
        case false:
            selectedCountry = self.arrCountries[indexPath.row]
        }
        
        super.pushToCountryWise(selectedCountry: selectedCountry)
    }
}
