//
//  GlobalSummary.swift
//  Corona-Info
//
//  Created by Mac Book on 01/05/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper

class GlobalSummary: BaseController {

    @IBOutlet weak var lblNewConfirmed: UILabel!
    @IBOutlet weak var lblTotalConfirmed: UILabel!
    @IBOutlet weak var lblNewDeaths: UILabel!
    @IBOutlet weak var lblTotalDeaths: UILabel!
    @IBOutlet weak var lblNewRecovered: UILabel!
    @IBOutlet weak var lblTotalRecovered: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getGlobalSummary()
        // Do any additional setup after loading the view.
    }
}
//MARK:- Services
extension GlobalSummary{
    private func setData(data:SummaryModel){
        self.lblNewConfirmed.text = "\(data.recentConfirmed)"
        self.lblTotalConfirmed.text = "\(data.totalConfirmed)"
        self.lblNewDeaths.text = "\(data.recentDeaths)"
        self.lblTotalDeaths.text = "\(data.totalDeaths)"
        self.lblNewRecovered.text = "\(data.recentRecovered)"
        self.lblTotalRecovered.text = "\(data.totalRecovered)"
    }
}
//MARK:- Services
extension GlobalSummary{
    private func getGlobalSummary(){
        APIManager.sharedInstance.usersAPIManager.GetSummary(params: [:], success: { (responseObject) in
            let response = responseObject as Dictionary
            if let Global = response["Global"] as? [String : Any]{
                guard let summary = Mapper<SummaryModel>().map(JSON: Global) else{return}
                self.setData(data: summary)
            }
        }) { (error) in
            print(error)
        }
    }
}
