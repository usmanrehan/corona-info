//
//  CountryWise.swift
//  Corona-Info
//
//  Created by Mac Book on 01/05/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper

class CountryWise: BaseController {
    
    @IBOutlet weak var lblActive: UILabel!
    @IBOutlet weak var lblRecovered: UILabel!
    @IBOutlet weak var lblDeaths: UILabel!
    @IBOutlet weak var lblConfirmed: UILabel!
    
    var selectedCountry: CountryModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCountryWiseSummary()
        // Do any additional setup after loading the view.
    }

}
//MARK:- Helper Methods
extension CountryWise{
    private func setData(data:CountryWiseModel){
        self.lblActive.text = "\(data.active)"
        self.lblRecovered.text = "\(data.recovered)"
        self.lblDeaths.text = "\(data.deaths)"
        self.lblConfirmed.text = "\(data.confirmed)"
    }
}
//MARK:- Services
extension CountryWise{
    private func getCountryWiseSummary(){
        let country = (self.selectedCountry?.Country ?? "").replacingOccurrences(of: " ", with: "+")
        APIManager.sharedInstance.usersAPIManager.GetCountryWiseSummary(country: country, success: { (responseObject) in
            print(responseObject)
            guard let countrySummary = responseObject as? [[String:Any]] else {return}
            let summary = Mapper<CountryWiseModel>().mapArray(JSONArray: countrySummary)
            self.setData(data: summary.last ?? CountryWiseModel())
        }) { (error) in
            print(error)
        }
    }
}
