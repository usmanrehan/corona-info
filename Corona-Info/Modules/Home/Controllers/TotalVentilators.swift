//
//  TotalVentilators.swift
//  Corona-Info
//
//  Created by Mac Book on 01/05/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper

class TotalVentilators: BaseController {

    @IBOutlet weak var lblTotalVentilators: UILabel!
    @IBOutlet weak var lblRequiredVentilators: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getPakistanSummary()
        // Do any additional setup after loading the view.
    }
    

}
//MARK:- Helper Methods
extension TotalVentilators{
    private func setData(data:CountryWiseModel){
        let ventilatorRequired = (((Double(data.active)/4)/(Double(3800)))*100.0).rounded()
        self.lblRequiredVentilators.text = "\(ventilatorRequired)%"
    }
}
extension TotalVentilators{
    private func getPakistanSummary(){
        let country = "Pakistan"
        APIManager.sharedInstance.usersAPIManager.GetCountryWiseSummary(country: country, success: { (responseObject) in
            print(responseObject)
            guard let countrySummary = responseObject as? [[String:Any]] else {return}
            let summary = Mapper<CountryWiseModel>().mapArray(JSONArray: countrySummary)
            self.setData(data: summary.last ?? CountryWiseModel())
        }) { (error) in
            print(error)
        }
    }
}
