
import Foundation

enum Strings: String{
    //MARK:- Notifies
    case ALERT = "Alert"
    case ERROR = "Error"
    case UNKNOWN_ERROR = "Unknown error"
    case YES = "Yes"
    case NO = "No"
    case OK = "Ok"
    case SUCCESS = "Success"
    case Confirmation = "Confirmation"
    case VERIFIED = "Verified"
    case PIN_SENT_EMAIL = "A verification PIN has been sent to your provided email address."
    
    //MARK:- Validation
    case EMPTY_LOGIN_FIELDS = "Please provide your valid email address or mobile number to login."
    case PROFILE_IMAGE_REQUIRED = "Please upload your profile picture."
    case INVALID_NAME = "Name should contain atleast 3 characters."
    case INVALID_EMAIL = "Please provide a valid Email Address."
    case INVALID_PHONE = "Please provide a valid Phone Number."
    case INVALID_OTP = "Please provide a valid OTP Code."
    case INVALID_COUNTRY = "Please select your country."
    case EMPTY_PWD = "Please provide password."
    case EMPTY_CONFIRM_PWD = "Please confirm your password."
    case INVALID_PWD = "Password should contain atleast 6 characters."
    case PWD_ATLEAST_SIX_CH = "At least 6 characters."
    case PWD_DONT_MATCH = "New password and confirm password does not match."
    case ALL_FIELD_REQ = "All Fields are required!"
    case INVALID_PIN = "Please provide complete PIN."
    case PWD_CHANGED = "You have successfully updated your password."
    case URL_NOT_VALID = "Provided URL is not valid."
    case LOGOUT = "Logout"
    case ASK_LOGOUT = "Are you sure you want to logout?"
    case PLEASE_AGREE_TO_TNC = "Please agree to our terms and conditions."
    
    case ERROR_GENERIC_MESSAGE = "Unable to connect server\n Please check your internet connection and try again later."
    case TOKEN_EXPIRED = "Invalid Authentication Token Supplied."
    case PASSWORD_UPDATED = "Password updated successfully."
    case PROFILE_UPDATED = "Profile updated successfully."
    
    case ARRIVED_AT_PICKUP = "Have you arrived at the pick-up?"
    case ASK_START_RIDE = "Are you sure you want to start this ride?"
    case ASK_END_RIDE = "Are you sure you want to end this ride?"
    case ASK_ACCEPT_RIDE = "Are you sure you want to accept this ride?"
    case ASK_REJECT_RIDE = "Are you sure you want to reject this ride?"
    case PASSWORD_CHANGED = "Password changed successfully\n Please re-login to continue using Transmissito."
    
    //MARK:- Error messages
    case RESPONSE_ERROR = "Invalid response for route:"
    
    //SingleMethod For the Usage of Localization.
    var text: String { return NSLocalizedString( self.rawValue, comment: "") }
    
    //App level strings
    case HIRE_FLEET = "Hire fleet"
    case ASK_TO_HIRE_FLEET = "Are you sure you want to hire this fleet?"
    
    //MARK:- Controllers title
    case HOME = "Home"
    case YOUR_RIDES = "Your Rides"
    case BOOKINGS = "Bookings"
    case CURRENT_BOOKINGS = "Current"
    case PREVIOUS_BOOKINGS = "Previous"
    case PENDING_BOOKINGS = "Pending"
    case ABOUT_US = "About Us"
    case NOTIFICATIONS = "Notifications"
    case HELP = "Help"
    case SETTINGS = "Settings"
    case FLEETS = "Fleets"
    case FLEET_DETAILS = "Fleet Details"
    case CUSTOMER_RATING = "Customer Rating"
    case YOUR_RIDE = "Your Ride"
    case START_RIDE = "Start Ride"
    case PROFILE = "Profile"
    case EARNINGS = "Earnings"
    case RIDES    = "Rides"
    case TRANSACTIONS = "Transactions"
    case BANKING_DETAILS = "Banking Details"
    
    //MARK:- Settings options
    case UPDATE_PROFILE = "Update profile"
    case NAME = "Name"
    case MOBILE = "Mobile"
    case EMAIL = "Email"
    case CHANGE_PASSWORD = "Change password"
    case ADD_BANKING_DETAILS = "Add banking details"
    case OLD_PASSWORD = "Old password"
    case NEW_PASSWORD = "New password"
    case CONFIRM_PASSWORD = "Confirm password"
    
    //MARK:- Button titles
    case LETS_GO = "Let's Go!"
    
    case NO_NOTIFICATIONS = "No notifications available."
    case NO_NOTIFICATIONS_DESC = "When you have notifications, you'll see them here."
    case NO_RIDES = "No bookings available."
    case NO_RIDES_DESC = "When you have bookings, you'll see them here."
    case NO_HELP = "No help available."
    case NO_HELP_DESC = "When you have help guide, you'll see them here."
    case NO_CERTIFICATES = "Currency certificate"
    case NO_CERTIFICATES_DESC = "Please attach your currency certificate(s)."
    case REPORT = "Report"
    case REPORT_DESC = "Are you sure you want to report this ride?"
    case DELETE_NOTIFICATIONS = "Are you sure you want to delete all the notifications?"
    
    //MARK:- RIDES STATUS
    case APPROVED = "Approved"
    case ARRIVED = "Arrived"
    //case PENDING    = "Pending"
    case COMPLETED = "Completed"
    case CANCEL = "Cancel"
    
    case PHOTO_ALREADY_ADDED = "Photo already added."
    case VIDEO_ALREADY_ADDED = "Video already added."
    case DOCUMENT_ALREADY_ADDED = "Document already added."
    
    case PHOTO_ADDED_SUCCESSFULLY = "Photo added successfully."
    case VIDEO_ADDED_SUCCESSFULLY = "Video added successfully."
    case DOCUMENT_ADDED_SUCCESSFULLY = "Document added successfully."
    
    case ATTACH_CURRENCY_OF_CERTIFICATE = "Please attach your currency of certificate."
    
    case INVALID_EMAIL_CODE = "Please enter complete verification code from email we sent you."
    case INVALID_PHONE_CODE = "Please enter complete verification code from phone number we sent you."
    
    case RESENT = "Code Resent"
    case RESENT_MESSAGE = "Code has been sent to your provided email address and phone number."
    case RESENT_MESSAGE_EMAIL = "Code has been sent to your provided email address."
    
    case INVALID_PROVIDED_NUMBER = "Provided number is not valid."
    case ESTIMATED_AMOUNT = "Estimated Amount"
    
    case PULL_TO_REFRESH = "Pull to refresh"
    case CURRENCY = "Rs"
    
    case PAY_NOW = "Pay now"
    case PAY_BY_WALLET = "Pay by wallet"
    
    case PENDING = "pending"
    case ACCEPTED = "accepted"
    case FINISHED = "finished"
    case CANCELLED = "cancelled"
    case INPROGRESS = "inProgress"

}
