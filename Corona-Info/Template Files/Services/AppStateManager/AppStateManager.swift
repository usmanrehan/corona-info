
import Foundation
import UIKit
import RealmSwift

class AppStateManager: NSObject {
    
    static let sharedInstance = AppStateManager()
    var loggedInUser: UserModel!
    var realm: Realm!
    
    override init() {
        super.init()
        if(!(realm != nil)){
            realm = try! Realm()
        }
        loggedInUser = realm.objects(UserModel.self).first
    }
    
    func isUserLoggedIn() -> Bool{
        if (self.loggedInUser) != nil {
            if self.loggedInUser.isInvalidated {
                return false
            }
            return true
        }
        return false
    }
    func loginUser(user:UserModel) {
        try! Global.APP_REALM?.write(){
            AppStateManager.sharedInstance.loggedInUser = user
            Global.APP_REALM?.add(user, update: .all)
        }
        AppDelegate.shared.changeRootViewController()
    }
    func logoutUser(){
        Utility.main.showAlert(message: Strings.ASK_LOGOUT.text, title: Strings.LOGOUT.text, controller: Utility.main.topViewController()!) { (yes, no) in
            if yes != nil{
                try! Global.APP_REALM?.write() {
                    Global.APP_REALM?.delete(self.loggedInUser)
                    self.loggedInUser = nil
                }
                AppDelegate.shared.changeRootViewController()
            }
        }
    }
    func deleteLoggedInUser(){
        try! Global.APP_REALM?.write() {
            Global.APP_REALM?.delete(self.loggedInUser)
            self.loggedInUser = nil
        }
    }
}
