
import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    //MARK:- /GetCountries
    func GetCountries(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        Utility.showLoader()
        let route: URL = URLforRoute(route: Route.Countries.rawValue, params: [:])! as URL
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- /GetSummary
    func GetSummary(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        Utility.showLoader()
        let route: URL = URLforRoute(route: Route.Summary.rawValue, params: [:])! as URL
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- /GetCountryWiseSummary
    func GetCountryWiseSummary(country: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        Utility.showLoader()
        let route: URL = URLforRoute(route: Route.CountryWiseSummary.rawValue+"\(country)/status/confirmed", params: [:])! as URL
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- /GetWorldCases
    func GetWorldCases(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        Utility.showLoader()
        let route: URL = URLforRoute(route: Route.WorldCases.rawValue, params: [:])! as URL
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
}
