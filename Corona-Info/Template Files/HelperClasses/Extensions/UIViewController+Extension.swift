
import UIKit

extension UIViewController {
    // Not using static as it wont be possible to override to provide custom storyboardID then
    class var storyboardID : String {
        //if your storyboard name is same as ControllerName uncomment it
        //return "\(self)"
        return "\(self)" + "_ID"
    }
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
}
extension UIViewController {
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!;
    }
}
//extension UIViewController {
//    func setStatusBarStyle(_ style: UIStatusBarStyle) {
//        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusBar.backgroundColor = style == .lightContent ? Global.APP_COLOR : Global.APP_COLOR_LIGHT
//            statusBar.setValue(style == .lightContent ? UIColor.white : .black, forKey: "foregroundColor")
//        }
//    }
//}
