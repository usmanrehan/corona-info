    import Foundation
    
    class ValidationError: Error {
        var message: String
        
        init(_ message: String) {
            self.message = message
        }
    }
    
    protocol ValidatorConvertible {
        func validated(_ value: String) throws -> String
    }
    
    enum ValidatorType {
        case email
        case password
        case newPassword
        case username
        case projectIdentifier
        case requiredField(field: String)
        case state
        case age
        case vehicleReg
        case vehicleInsurance
        case currencyCertification
        case drivingLicense
        case phoneNumber
    }
    
    enum VaildatorFactory {
        static func validatorFor(type: ValidatorType) -> ValidatorConvertible {
            switch type {
            case .email: return EmailValidator()
            case .password: return PasswordValidator()
            case .newPassword: return NewPasswordValidator()
            case .username: return UserNameValidator()
            case .projectIdentifier: return ProjectIdentifierValidator()
            case .requiredField(let fieldName): return RequiredFieldValidator(fieldName)
            case .state: return StateValidator()
            case .age: return AgeValidator()
            case .vehicleReg: return VehicleRegValidator()
            case .vehicleInsurance: return VehicleInsuranceValidator()
            case .currencyCertification: return CertificationValidator()
            case .drivingLicense: return LicenseValidator()
            case .phoneNumber: return PhoneNumberValidator()
            }
        }
    }
    
    //"J3-123A" i.e
    struct ProjectIdentifierValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            do {
                if try NSRegularExpression(pattern: "^[A-Z]{1}[0-9]{1}[-]{1}[0-9]{3}[A-Z]$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw ValidationError("Invalid Project Identifier Format")
                }
            } catch {
                throw ValidationError("Invalid Project Identifier Format")
            }
            return value
        }
    }
    
    struct PhoneNumberValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            do {
                if !Validation.isValidPhoneNumber(value) {
                    throw ValidationError("Invalid phone number")
                }
            } catch {
                throw ValidationError("Invalid phone number")
            }
            return value
        }
    }
    
    struct RequiredFieldValidator: ValidatorConvertible {
        private let fieldName: String
        
        init(_ field: String) {
            fieldName = field
        }
        
        func validated(_ value: String) throws -> String {
            guard !value.isEmpty else {
                throw ValidationError("Required field " + fieldName)
            }
            return value
        }
    }
    
    struct UserNameValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value.count >= 3 else {
                throw ValidationError("Name must contain more than three characters" )
            }
            guard value.count < 18 else {
                throw ValidationError("Name shouldn't contain more than 18 characters" )
            }
            
//            do {
//                if try NSRegularExpression(pattern: "^[a-z]{1,18}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
//                    throw ValidationError("Invalid username, username should not contain whitespaces, numbers or special characters")
//                }
//            } catch {
//                throw ValidationError("Invalid username, username should not contain whitespaces,  or special characters")
//            }
            return value
        }
    }
    
    struct PasswordValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value != "" else {throw ValidationError("Password is required")}
            //guard value.count >= 6 else { throw ValidationError("Password must have at least 6 characters") }
            
            //        do {
            //            if try NSRegularExpression(pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
            //                throw ValidationError("Password must be more than 6 characters, with at least one character and one numeric character")
            //            }
            //        } catch {
            //            throw ValidationError("Password must be more than 6 characters, with at least one character and one numeric character")
            //        }
            return value
        }
    }
    
    struct NewPasswordValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value != "" else {throw ValidationError("Please set your new password")}
            //guard value.count >= 6 else { throw ValidationError("Password must have at least 6 characters") }
            
            //        do {
            //            if try NSRegularExpression(pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
            //                throw ValidationError("Password must be more than 6 characters, with at least one character and one numeric character")
            //            }
            //        } catch {
            //            throw ValidationError("Password must be more than 6 characters, with at least one character and one numeric character")
            //        }
            return value
        }
    }
    
    struct EmailValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            do {
                if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw ValidationError("Invalid e-mail Address")
                }
            } catch {
                throw ValidationError("Invalid e-mail Address")
            }
            return value
        }
    }
    
    struct StateValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            do {
                if !Validation.validateStringLength(value) {
                    throw ValidationError("Please select your state")
                }
            } catch {
                throw ValidationError("Please select your state")
            }
            return value
        }
    }
    
    class AgeValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value.count > 0 else {throw ValidationError("Age is required")}
            guard let age = Int(value) else {throw ValidationError("Age must be a number!")}
            guard value.count < 3 else {throw ValidationError("Invalid age number!")}
            guard age >= 18 else {throw ValidationError("You have to be over 18 years old to user our app :)")}
            return value
        }
    }
    struct VehicleRegValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value != "" else {throw ValidationError("Vehicle Registration Number is Required")}
            return value
        }
    }
    struct VehicleInsuranceValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value != "" else {throw ValidationError("Vehicle Insurance is Required")}
            return value
        }
    }
    struct CertificationValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value != "" else {throw ValidationError("Currency of Certification is Required")}
            return value
        }
    }
    struct LicenseValidator: ValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value != "" else {throw ValidationError("Driving is Required")}
            return value
        }
    }
