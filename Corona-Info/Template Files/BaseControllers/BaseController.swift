
import Foundation
import UIKit

class BaseController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigation()
    }
}
//MARK:- Add Navigation Items
extension BaseController{
    private func addBackBarButtonItem() {
        let image = UIImage(named: "back")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.onBtnBack))
        
        self.navigationItem.leftBarButtonItem = backItem
    }
}
//MARK:- Selectors
extension BaseController{
    @objc func onBtnBack() {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- Navigation Methods
extension BaseController{
    func pushToGlobalSummary(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "GlobalSummary") as! GlobalSummary
        controller.title = "Covid19 Global Summary"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToCountries(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Countries") as! Countries
        controller.title = "Country-Wise Situation"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToCountryWise(selectedCountry:CountryModel){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CountryWise") as! CountryWise
        controller.title = "\(selectedCountry.Country)'s Situation"
        controller.selectedCountry = selectedCountry
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToCoronaVirusSpreading(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CoronaVirusSpreading") as! CoronaVirusSpreading
        controller.title = "Corona Virus Spreading"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToVentilatorStatus(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "TotalVentilators") as! TotalVentilators
        controller.title = "Ventilator Status"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToVaccines(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Vaccines") as! Vaccines
        controller.title = "Vaccines"
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK:- Helper Methods
extension BaseController{
    private func resetNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = Global.APP_COLOR
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    private func setNavigation(){
        guard let navControllerCount = self.navigationController?.viewControllers.count else {return}
        if navControllerCount > 1 {
            self.addBackBarButtonItem()
        }
        self.resetNavigationBar()
    }
}
